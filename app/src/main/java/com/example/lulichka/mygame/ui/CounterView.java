package com.example.lulichka.mygame.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lulichka.mygame.R;

public class CounterView extends RelativeLayout {

    private TextView counter;

    public CounterView(Context context) {
        this(context, null);
    }

    public CounterView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CounterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        inflate(context, R.layout.counter, this);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CounterView,
                0, 0);

        ImageView icon = (ImageView) findViewById(R.id.icon);
        counter = (TextView) findViewById(R.id.counter);
        ImageView background = (ImageView) findViewById(R.id.iv_feature_background);

        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Cartwheel.otf");
        counter.setTypeface(tf);

        try {
            int imageRes = a.getResourceId(R.styleable.CounterView_img_res, 0);
            boolean hasCounter = a.getBoolean(R.styleable.CounterView_has_counter, false);
            icon.setImageResource(imageRes);
            if (!hasCounter) {
                background.setImageResource(R.drawable.bg_feature);
                counter.setVisibility(GONE);
            }
        } finally {
            a.recycle();
        }
    }

    public void setText(String text) {
        counter.setText(text);
    }

}
