package com.example.lulichka.mygame.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PreferenceHelper {
    private static final String SOUND_ENABLED = "SOUND_ENABLED";
    private static final String CATEGORY_SOLVED = "CATEGORY_SOLVED";
    private static PreferenceHelper INSTANCE;
    private final SharedPreferences preferences;

    PreferenceHelper(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static PreferenceHelper getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("PreferenceHelper wasn't initialized");
        }
        return INSTANCE;
    }

    public static void init(Context context) {
        INSTANCE = new PreferenceHelper(context);
    }

    private SharedPreferences.Editor getEditor() {
        return preferences.edit();
    }


    public boolean isSoundEnabled() {
        return preferences.getBoolean(SOUND_ENABLED, true);
    }

    public void setSoundEnabled(boolean enabled) {
        getEditor().putBoolean(SOUND_ENABLED, enabled).apply();
    }
//todo make or delete
    public void setCategorySolved(boolean solved) {
        getEditor().putBoolean(CATEGORY_SOLVED, solved).apply();
    }

    public boolean isCategorySolved() {return preferences.getBoolean(CATEGORY_SOLVED, false);
    }

}
