package com.example.lulichka.mygame;

import android.app.Application;

import com.example.lulichka.mygame.user.ItemsInventory;
import com.example.lulichka.mygame.util.PreferenceHelper;
import com.example.lulichka.mygame.util.SoundHelper;


public class CoreApp extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        //FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
        PreferenceHelper.init(this);
        ItemsInventory.init(this);
        SoundHelper.getInstance(this);
    }
}
