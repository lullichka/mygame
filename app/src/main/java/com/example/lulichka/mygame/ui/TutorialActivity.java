package com.example.lulichka.mygame.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.lulichka.mygame.R;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TutorialActivity extends AppCompatActivity {

    @BindView(R.id.tutorial_slider)
    ViewPager tutorialSlider;
    @BindView(R.id.tutorial_pager_indicator)
    CirclePageIndicator pageIndicator;
    @BindView(R.id.tutorial_next_button)
    ImageView nextSlide;
    @BindView(R.id.tutorial_prev_button)
    ImageView prevSlide;
    int currentSlide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);

        tutorialSlider.setAdapter(new TutorialPager(getSupportFragmentManager()));
        pageIndicator.setViewPager(tutorialSlider);
        nextSlide.setOnClickListener(this::nextSlide);
        prevSlide.setOnClickListener(this::prevSlide);
        tutorialSlider.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentSlide = position;
                changeButtonsImages(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    private void changeButtonsImages(int position) {
        switch (position) {
            case 0:
                setImage(prevSlide, 0);
                setImage(nextSlide, R.drawable.ic_next_slide);
                break;
            case 1:
                setImage(prevSlide, R.drawable.ic_prev_slide);
                setImage(nextSlide, R.drawable.ic_next_slide);
                break;
            case 2:
                setImage(prevSlide, R.drawable.ic_prev_slide);
                setImage(nextSlide, R.drawable.ic_next_slide);
                break;
            case 3:
                setImage(prevSlide, R.drawable.ic_prev_slide);
                setImage(nextSlide, R.drawable.ic_close_tutorial);
                break;
        }
    }

    private void setImage(ImageView nextSlide, int imageRes) {
        Glide.with(TutorialActivity.this)
                .load(imageRes)
                .into(nextSlide);
    }

    private void prevSlide(View view) {
        if (currentSlide != 0) {
            tutorialSlider.setCurrentItem(--currentSlide);
        }
    }

    private void nextSlide(View view) {
        if (currentSlide == 2) {
            GameActivity.start(this);
            finish();
        }
        tutorialSlider.setCurrentItem(++currentSlide);
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, TutorialActivity.class);
        context.startActivity(starter);
    }

    static class TutorialPager extends FragmentStatePagerAdapter {
        List<SlideHolder> fragments;

        public TutorialPager(FragmentManager fragmentManager) {
            super(fragmentManager);
            fragments = new ArrayList<>();
            fragments.add(SlideHolder.newInstance(R.drawable.slide_1_new));
            fragments.add(SlideHolder.newInstance(R.drawable.slide_2_new));
            fragments.add(SlideHolder.newInstance(R.drawable.slide_3_new));
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }
    }

    public static class SlideHolder extends Fragment {

        public static final String IMAGE_RES = "IMAGE_RES";
        private static final int DEFAULT_ANIMATION_DURATION = 2000;

        public static SlideHolder newInstance(@DrawableRes int imageRes) {
            Bundle args = new Bundle();
            args.putInt(IMAGE_RES, imageRes);

            SlideHolder fragment = new SlideHolder();
            fragment.setArguments(args);
            return fragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater,
                                 @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_tutorial_slide, container, false);

            ImageView slideImage = (ImageView) view.findViewById(R.id.slide);
            ImageView twoImage =  (ImageView) view.findViewById(R.id.two);

            Glide.with(this)
                    .load(getArguments().getInt(IMAGE_RES))
                    .into(slideImage);
            if (getArguments().getInt(IMAGE_RES)==R.drawable.slide_1_new){
                twoImage.setVisibility(View.VISIBLE);
                Animation anim = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                        R.anim.appearing_rotation);
                twoImage.startAnimation(anim);
            }
            return view;
        }
    }

    @Override
    public void onBackPressed() {
        GameActivity.start(this);
        super.onBackPressed();
    }
}
