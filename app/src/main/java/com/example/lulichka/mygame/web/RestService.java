package com.example.lulichka.mygame.web;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;


public class RestService {
    public static WebAPI Service = init();

    private static WebAPI init() {
        OkHttpClient client = new OkHttpClient.Builder().build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://luul.cbcmrb.com")
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        return retrofit.create(WebAPI.class);

    }

    public interface WebAPI {
        @GET("/json1/question_response")
        Call<QuestionsResponse> getQuestions();
    }
}
