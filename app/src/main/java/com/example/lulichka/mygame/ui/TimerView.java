package com.example.lulichka.mygame.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lulichka.mygame.R;
import com.example.lulichka.mygame.messages.TimerMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimerView extends FrameLayout {

    @BindView(R.id.image_timer) ImageView timer;
    @BindView(R.id.timer_counter) TextView timerCounter;
    @BindView(R.id.frozen_background) ImageView frozenBackground;
    private int secondLeft;

    public TimerView(Context context) {
        this(context, null);
    }

    public TimerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = inflate(context, R.layout.view_timer, this);
        ButterKnife.bind(view, this);
        EventBus.getDefault().register(this);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Skranji-Bold.ttf");
        timerCounter.setTypeface(tf);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);
    }

    public void setTime(int secondLeft) {
        this.secondLeft = secondLeft;
        timerCounter.setText(String.format("%01d:%02d", secondLeft / 60, secondLeft % 60));
    }

    public void setProperTimerColor() {
        boolean isLastTenSeconds = secondLeft <= 10;
        timerCounter.setTextColor(isLastTenSeconds ? 0xffd81111 : 0xffffffff);
        Glide.with(getContext())
                .load(isLastTenSeconds ? R.drawable.ic_timer_last_ten_seconds : R.drawable.ic_timer)
                .into(timer);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTimerTick(TimerMessage message) {
        secondLeft = message.getSecondsLeft();
        setTime(secondLeft);
        if (message.isFrozen()) {
            frozenBackground.setVisibility(View.VISIBLE);
            timerCounter.setTextColor(0xff57e7ff);
            Glide.with(getContext())
                    .load(R.drawable.ic_timer_frozen)
                    .into(timer);
        } else {
            frozenBackground.setVisibility(GONE);
            setProperTimerColor();
        }
    }

}
