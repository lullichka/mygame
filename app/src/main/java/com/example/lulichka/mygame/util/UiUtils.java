package com.example.lulichka.mygame.util;

import android.app.ProgressDialog;
import android.content.Context;

public class UiUtils {

    private static ProgressDialog progressDialog;

    public static void showLoading(Context context) {
        if (context == null) {
            return;
        }
        hideLoading();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void hideLoading() {
        if (progressDialog != null
                && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
