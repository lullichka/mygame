package com.example.lulichka.mygame.ui;

import android.content.Context;
import android.support.transition.TransitionManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lulichka.mygame.R;
import com.example.lulichka.mygame.messages.WordSolvedMessage;
import com.example.lulichka.mygame.web.QuestionsResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDimen;
import butterknife.BindView;


public class GameField extends LinearLayout {

    private final RelativeLayout answerContainer;
     private final LinearLayout gameFieldContainer;

    @BindView(R.id.word_container)
    GridLayout wordContainer;

    @BindDimen(R.dimen.letter_width)
    float letterWidth;

    List<View> lettersViews;
    List<View> answerLetterViews;

    StringBuffer resultWord = new StringBuffer("");

    private static final String TAG = GameField.class.getSimpleName();

    private QuestionsResponse.Data.Category.Word fieldWord;
    private boolean solved;

    public GameField(Context context) {
        this(context, null);
    }

    public GameField(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GameField(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = inflate(context, R.layout.fragment_game_field, this);
        gameFieldContainer = (LinearLayout)view.findViewById(R.id.game_field_container);
        wordContainer = (GridLayout) view.findViewById(R.id.word_container);
        answerContainer = (RelativeLayout) view.findViewById(R.id.answer_container);
        lettersViews = new ArrayList<>();
        answerLetterViews = new ArrayList<>();
    }

    private View createView(LayoutInflater inflater, String letter1) {
        View view = inflater.inflate(R.layout.item_letter, wordContainer, false);
        TextView letterLabel = (TextView) view.findViewById(R.id.label_letter);
        letterLabel.setText(letter1);
        view.setTag(letter1);
        lettersViews.add(view);
        view.findViewById(R.id.letter).setOnTouchListener(createTouchListener(view));
        return view;
    }

    private View.OnTouchListener createTouchListener(View view) {
        final boolean visible;
        return (v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    v.setSelected(true);
                    resultWord = resultWord.append(view.getTag());
                    Log.d(TAG, resultWord.toString());
                    if (checkAnswer()) {
                        onWordSolved();
                        //answerContainer.setVisibility(VISIBLE);
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_UP:
            }
            return true;
        };
    }

    private void setUpLetters(LayoutInflater inflater, String word) {
        int column;
        if (word.length() > 4) {
            column = 3;
        } else column = 2;
        int row = word.length() / column;
        wordContainer.setColumnCount(column);
        wordContainer.setRowCount(row + 1);
        for (int i = 0; i < word.length(); i++) {
            String letter = String.valueOf(word.charAt(i)).toUpperCase();
            wordContainer.addView(createView(inflater, letter));
        }
        for (int j = 0; j < getCorrectWord().length(); j++) {
            String letter = String.valueOf(getCorrectWord().charAt(j)).toUpperCase();
            answerContainer.addView(createAnswerView(inflater, letter));
        }
    }

    private View createAnswerView(LayoutInflater inflater, String letter) {
        View view = inflater.inflate(R.layout.item_letter, answerContainer, false);
        TextView letterLabel = (TextView) view.findViewById(R.id.label_letter);
        letterLabel.setText(letter);
        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setId(answerLetterViews.size() + 1000);
        if (!answerLetterViews.isEmpty()) {
            int location = answerLetterViews.size() - 1;
            layoutParams.addRule(RelativeLayout.RIGHT_OF, answerLetterViews.get(location).getId());
        }
        view.setLayoutParams(layoutParams);
        answerLetterViews.add(view);
        return view;
    }

    public void setWord(QuestionsResponse.Data.Category.Word word) {
        this.fieldWord = word;
        setUpLetters(LayoutInflater.from(getContext()), this.getWord());
    }

    public String getWord() {
        return fieldWord.word;
    }

    public String getCorrectWord() {
        return fieldWord.word_correct;
    }

    boolean checkAnswer() {
        String answer = getCorrectWord();
        if (resultWord.length() == answer.length()) {
            if (String.valueOf(resultWord).equalsIgnoreCase(answer)) {
                return true;
            } else {
                Toast.makeText(getContext(), "Try again", Toast.LENGTH_LONG).show();
                resultWord.delete(0, resultWord.length());
            }
        }
        return false;
    }

    private void onWordSolved() {
        boolean visible = true;
        setSolved(true);
        notifyWordCompleted();
        TransitionManager.beginDelayedTransition(gameFieldContainer);
        visible = !visible;
        answerContainer.setVisibility(visible ? View.GONE : View.VISIBLE);
        wordContainer.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    void notifyWordCompleted() {
        WordSolvedMessage wordSolvedMessage = new WordSolvedMessage();
        wordSolvedMessage.solvedWordId = fieldWord.id;
        EventBus.getDefault().post(wordSolvedMessage);
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }
}

