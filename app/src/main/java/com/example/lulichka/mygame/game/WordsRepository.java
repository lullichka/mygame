package com.example.lulichka.mygame.game;

import android.util.SparseArray;

import com.example.lulichka.mygame.web.QuestionsResponse;
import com.example.lulichka.mygame.web.QuestionsResponse.Data.Category;
import com.example.lulichka.mygame.web.QuestionsResponse.Data.Category.Word;

import java.util.ArrayList;
import java.util.List;

public class WordsRepository {
    public static final String TAG = "WordRepository";
    private static WordsRepository INSTANCE = new WordsRepository();
    private final GameManager gameManager;
    private SparseArray<QuestionsResponse.Data.Category> gameWords = new SparseArray<>();

    private WordsRepository() {
        gameManager = GameManager.getInstance();
    }

    public static WordsRepository getInstance() {
        return INSTANCE;
    }

    public List<Word> getAllWordsForCurrentRound() {
        return gameManager.getCurrentCategory().words;
    }

    public void saveQuestions(List<QuestionsResponse.Data.Category> categories) {
        int categoriesCount = setCategoriesCount(categories);
        if (categoriesCount != 0) {
            for (int i = 0; i < categoriesCount; i++) {
                gameWords.put(i, categories.get(i));
            }
        }
    }

    //todo check size of catList
    protected int setCategoriesCount(List<Category> categories) {
        return categories.size();
    }

    public boolean isWordSolved(int id) {
        return getWordById(id).isSolved;
    }

    public void setWordSolved(int id) {
        getWordById(id).isSolved = true;
    }

    public Word getWordById(int id) {
        for (Word word : getAllWordsForCurrentRound()) {
            if (word.id == id) {
                return word;
            }
        }
        throw new IllegalArgumentException("Wrong word id: " + id);
    }

    Category getCategoryById(int categoryId) {
        for (Category category : getAllCategories()) {
            if (category.id == categoryId) {
                return category;
            }
        }
        throw new IllegalArgumentException("Wrong category id: " + categoryId);
    }

    public List<Category> getAllCategories() {
        List<Category> categoryList = new ArrayList<>();
        for (int i = 0; i < gameWords.size(); i++) {
            categoryList.add(gameWords.get(i));
        }
        return categoryList;
    }

    public boolean wasLastCategory() {
        return gameWords.size() == gameManager.getCurrentCategory().id;
    }

}
