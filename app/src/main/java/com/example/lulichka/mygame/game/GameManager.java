package com.example.lulichka.mygame.game;

import android.util.Log;

import com.example.lulichka.mygame.web.QuestionsResponse.Data.Category;
import com.example.lulichka.mygame.web.QuestionsResponse.Data.Category.Word;

import java.util.List;


public class GameManager {
    private static GameManager INSTANCE = new GameManager();
    private static final String TAG = GameManager.class.getSimpleName();
    private Word currentWord;
    private Category currentCategory;
    private List<Word> currentWordsList;
    private int currentWordIndex;
    private int gameCompensation;
    private int pointsToDisplay;
    private int roundPointsGain;

    public static GameManager getInstance() {
        return INSTANCE;
    }

    public Category getCurrentCategory() {
        return currentCategory;
    }

    public Word getCurrentWord() {
        return currentWord;
    }

    public void onWordSolved(int change) {
        Log.d(TAG, " mRoundPointsGain " + pointsToDisplay);
        roundPointsGain += change;
    }

    public void nextRound() {
        currentWordIndex++;
        setCurrentWord();
    }

    public boolean isLastRound() {
        return currentWordIndex == currentWordsList.size() - 1;
    }

    private void setCurrentWord() {
        currentWord = currentWordsList.get(currentWordIndex);
    }

    public void setGameCompensation(int gameCompensation) {
        Log.d(TAG, "setGameCompensation() called with: GameCompensation = [" + this.gameCompensation + "]");
        this.gameCompensation = gameCompensation;
    }

    public int getGameCompensation() {
        Log.d(TAG, "getGameCompensation() called " + gameCompensation);
        return gameCompensation;
    }

    public Integer getPointsToDisplay() {
        return pointsToDisplay;
    }

    public void setPointsToDisplay(int pointsToDisplay) {
        Log.d(TAG, "setPointsToDisplay() called with: pointsToDisplay = [" + this.pointsToDisplay + "]");
        this.pointsToDisplay = pointsToDisplay;
    }

    public void startNewGame(int categoryId) {
        WordsRepository wordsRepo = WordsRepository.getInstance();
        currentCategory = wordsRepo.getCategoryById(categoryId);
        setCurrentCategory(currentCategory);
        currentWordIndex = 0;
        currentWordsList = currentCategory.words;
        roundPointsGain = 0;
        setCurrentWord();
    }

    public int getCurrentWordIndex() {
        return currentWordIndex;
    }

    public int getRoundPointsGain() {
        return roundPointsGain;
    }

    public void setCurrentCategory(Category currentCategory) {
        this.currentCategory = currentCategory;
    }

    public int getNextCategoryId() {
        WordsRepository wordsRepository = WordsRepository.getInstance();
        if (!wordsRepository.wasLastCategory()){
            return   getCurrentCategory().id+1;
        }
        throw  new IllegalArgumentException("There was last category");
    }


}
