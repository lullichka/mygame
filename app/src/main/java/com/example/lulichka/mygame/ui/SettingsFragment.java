package com.example.lulichka.mygame.ui;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lulichka.mygame.R;
import com.example.lulichka.mygame.util.PreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsFragment extends Fragment {
    @BindView(R.id.iv_sound_indicator)
    ImageView buttonSound;
    @BindView(R.id.iv_sound_off_indicator)
    ImageView soundDisabled;
    @BindView(R.id.tv_sound)
    TextView disableSounds;
    private boolean soundEnabled;
    private PreferenceHelper preferenceHelper;

    @OnClick(R.id.iv_sound_indicator)
    public void setSoundDisabled() {
        soundEnabled = !soundEnabled;
        preferenceHelper.setSoundEnabled(soundEnabled);
        if (!soundEnabled) {
            soundDisabled.setVisibility(View.VISIBLE);
        } else soundDisabled.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_back)
    public void backToGame() {
        getActivity().findViewById(R.id.button_settings).setVisibility(View.VISIBLE);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, new GameFragment())
                .commit();
    }

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().findViewById(R.id.button_settings).setVisibility(View.GONE);
        preferenceHelper = PreferenceHelper.getInstance();
        soundEnabled = preferenceHelper.isSoundEnabled();
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Cartwheel.otf");
        disableSounds.setTypeface(tf);
        if (!soundEnabled) {
            soundDisabled.setVisibility(View.VISIBLE);
        }
        return view;
    }


}
