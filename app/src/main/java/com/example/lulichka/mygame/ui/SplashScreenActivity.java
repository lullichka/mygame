package com.example.lulichka.mygame.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.lulichka.mygame.R;

import butterknife.ButterKnife;

public class SplashScreenActivity extends AppCompatActivity {

    private static final int SPLASH_DURATION = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        new Handler().postDelayed(this::showLoginScreen, SPLASH_DURATION);
    }

    private void showLoginScreen() {
        GameActivity.start(this);
        finish();
    }

}
