package com.example.lulichka.mygame.util;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import java.io.IOException;


public class SoundHelper {
    private static SoundHelper INSTANCE;
    private Context context;
    private final SoundPool soundPool;
    private MediaPlayer mediaPlayer;
    private final int starSoundsId;
    private final int timerSoundsId;
    private int pointSoundId;
    private int countPlaySound;
    private int pointsPlaySound;
    private int starPlaySound;


    SoundHelper(Context context) {
        this.context = context;
        soundPool = new SoundPool(8, AudioManager.STREAM_MUSIC, 0);
        pointSoundId = loadMusic("music/coins.mp3");
        starSoundsId = loadMusic("music/stars.wav");
        timerSoundsId = loadMusic("music/timer.wav");
    }

    public static SoundHelper getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SoundHelper(context);
        }
        return INSTANCE;
    }

    private int loadMusic(String fileName) {
        AssetFileDescriptor musicFile = null;
        try {
            musicFile = context.getAssets().openFd(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return soundPool.load(musicFile, 0);
    }

    public void playStarsSound() {
        if (isSoundEnabled()) {
            playSound(starSoundsId, false);
        }
    }

    public void playPointSound() {
        if (isSoundEnabled()) {
            pointsPlaySound = playSound(pointSoundId, false);
        }
    }

    public void playBackgroundMusic() {
        if (!isSoundEnabled()) {
            return;
        }
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer = new MediaPlayer();
            AssetFileDescriptor afd = context.getAssets().openFd("music/track.mp3");
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.setVolume(0.7f, 0.7f);
            mediaPlayer.prepare();
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopBackgroundMusic() {
        if (mediaPlayer != null
                && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
    }

    public void playCountdown() {
        if (isSoundEnabled()) {
            countPlaySound = playSound(timerSoundsId, true);
        }
    }

    public void pauseCountdown() {
        soundPool.stop(countPlaySound);
        soundPool.stop(pointsPlaySound);
    }

    private int playSound(int soundId, boolean loop) {
        return soundPool.play(soundId, 1, 1, 0, loop ? -1 : 0, 1);
    }

    public void pauseAllSounds() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
        soundPool.autoPause();
    }

    public void resumeAllSounds() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }
        soundPool.autoResume();
    }

    private boolean isSoundEnabled() {
        return PreferenceHelper.getInstance().isSoundEnabled();
    }

}
