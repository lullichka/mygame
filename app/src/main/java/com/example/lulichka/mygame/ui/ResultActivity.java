package com.example.lulichka.mygame.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lulichka.mygame.R;
import com.example.lulichka.mygame.game.GameManager;
import com.example.lulichka.mygame.game.WordsRepository;
import com.example.lulichka.mygame.user.ItemsInventory;
import com.example.lulichka.mygame.util.SoundHelper;
import com.example.lulichka.mygame.web.QuestionsResponse;
import com.example.lulichka.mygame.web.QuestionsResponse.Data.Category.Word;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResultActivity extends AppCompatActivity {

    public static final int NEXT_GAME = 1;
    public static final int BACK_TO_MENU = 2;
    public static final int REPLAY = 3;
    private static final String CATEGORY_ID = "categoryId";

    @BindView(R.id.next_game)
    ImageView nextRound;
    @BindView(R.id.replay_category)
    ImageView replay;
    @BindView(R.id.back_to_menu)
    ImageView backToMenu;

    @BindDimen(R.dimen.small_star_padding)
    float smallStarPadding;
    @BindDimen(R.dimen.big_star_padding)
    float bigStarPadding;

    private List<QuestionsResponse.Data.Category.Word> solvedWords;
    private int categoryId;

    @OnClick(R.id.next_game)
    public void nextGame() {
        Intent intent = new Intent();
        intent.putExtra(CATEGORY_ID, categoryId);
        setResult(NEXT_GAME, intent);
        finish();
    }

    @OnClick(R.id.back_to_menu)
    public void backToMenu() {
        Intent intent = new Intent();
        intent.putExtra(CATEGORY_ID, categoryId);
        setResult(BACK_TO_MENU, intent);
        finish();
    }

    @OnClick(R.id.replay_category)
    public void replay() {
        Intent intent = new Intent();
        intent.putExtra(CATEGORY_ID, categoryId);
        setResult(REPLAY, intent);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_result);
        ButterKnife.bind(this);

        FrameLayout rootView = (FrameLayout) findViewById(R.id.root);
        RelativeLayout dialogHolder = (RelativeLayout) findViewById(R.id.dialog_holder);
        categoryId = getIntent().getIntExtra(CATEGORY_ID, 0);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setAttributes(lp);
        GameManager gameManager = GameManager.getInstance();
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Cartwheel.otf");
        TextView userScoresTitle = (TextView) findViewById(R.id.user_scores_title);
        TextView userScores = (TextView) findViewById(R.id.user_scores);
        TextView bestScoresTitle = (TextView) findViewById(R.id.best_scores_title);
        TextView bestScores = (TextView) findViewById(R.id.best_scores);
        LinearLayout starsHolder = (LinearLayout) findViewById(R.id.stars_holder);
        userScores.setTypeface(tf);
        userScores.setText(String.valueOf(gameManager.getRoundPointsGain()));
        userScoresTitle.setTypeface(tf);
        bestScores.setTypeface(tf);
        bestScoresTitle.setTypeface(tf);
        bestScores.setText(String.valueOf(gameManager.getRoundPointsGain()));
        this.solvedWords = new ArrayList<>();
        for (Word word : WordsRepository.getInstance().getAllWordsForCurrentRound()) {
            if (WordsRepository.getInstance().isWordSolved(word.id)) {
                this.solvedWords.add(word);
            }
        }
        if (allWordsSolved()){
            nextRound.setVisibility(View.VISIBLE);
        }

        RecyclerView solvedWordsRecycler = (RecyclerView) findViewById(R.id.solved_words_view);
        solvedWordsRecycler.setLayoutManager(new GridLayoutManager(this, 2));
        solvedWordsRecycler.setAdapter(new WordsAdapter());

        if (WordsRepository.getInstance().wasLastCategory()) {
            nextRound.setVisibility(View.GONE);
            replay.setVisibility(View.VISIBLE);
            backToMenu.setVisibility(View.VISIBLE);
        }
        ItemsInventory.getInstance().addPoints(gameManager.getRoundPointsGain());
        SoundHelper.getInstance(this).playStarsSound();
        showStarsAnimation(rootView, dialogHolder, starsHolder);
    }

    private boolean allWordsSolved() {
        return countSolvedWords(WordsRepository.getInstance().getAllWordsForCurrentRound())
                ==WordsRepository.getInstance().getAllWordsForCurrentRound().size();
    }

    private void showStarsAnimation(final FrameLayout rootView, final RelativeLayout dialogHolder, final LinearLayout starsHolder) {
        List<Word> allQuestions = WordsRepository.getInstance().getAllWordsForCurrentRound();
        ImageView star1 = (ImageView) findViewById(R.id.star1);
        ImageView star2 = (ImageView) findViewById(R.id.star2);
        ImageView star3 = (ImageView) findViewById(R.id.star3);
        int solvedWords = countSolvedWords(allQuestions);
        if (solvedWords > 0) {
            rootView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    rootView.getViewTreeObserver().removeOnPreDrawListener(this);
                    View starView = createStarView(rootView, R.layout.small_star);
                    float x = star1.getX() + starsHolder.getX() + dialogHolder.getX();
                    float y = star1.getY() + starsHolder.getY() + dialogHolder.getY() + smallStarPadding;
                    AnimatorSet set = getAnimatorSet(starView, x, y, -12);
                    if (solvedWords > 1) {
                        set.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                View starView = createStarView(rootView, R.layout.major_star);
                                float x = star2.getX() + starsHolder.getX() + dialogHolder.getX();
                                float y = star2.getY() + starsHolder.getY() + dialogHolder.getY() + bigStarPadding;
                                AnimatorSet animatorSet = getAnimatorSet(starView, x, y, 0);
                                if (solvedWords == WordsRepository.getInstance().getAllWordsForCurrentRound().size()) {
                                    animatorSet.addListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            View starView = createStarView(rootView, R.layout.small_star);
                                            float x = star3.getX() + starsHolder.getX() + dialogHolder.getX();
                                            float y = star3.getY() + starsHolder.getY() + dialogHolder.getY() + smallStarPadding;
                                            getAnimatorSet(starView, x, y, 12).start();
                                        }
                                    });
                                }
                                animatorSet.start();
                            }
                        });
                    }
                    set.start();
                    return true;
                }
            });
        }
    }

    private int countSolvedWords(List<QuestionsResponse.Data.Category.Word> words) {
        WordsRepository wordsRepository = WordsRepository.getInstance();
        int counter = 0;
        for (Word word : words) {
            if (wordsRepository.isWordSolved(word.id)) {
                counter++;
            }
        }
        return counter;
    }


    @NonNull
    private View createStarView(FrameLayout rootView, int small_star) {
        View starView = getLayoutInflater().inflate(small_star, rootView, false);
        rootView.addView(starView);

        starView.setX(rootView.getWidth() / 2 - starView.getWidth() / 2);
        starView.setY(rootView.getHeight() + starView.getHeight());
        starView.setScaleX(3);
        starView.setScaleY(3);
        starView.setRotation(800);
        return starView;
    }

    @NonNull
    private AnimatorSet getAnimatorSet(View starView, float x, float y, int angle) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator xAnimator = ObjectAnimator
                .ofFloat(starView, "x", x);
        ObjectAnimator yAnimator = ObjectAnimator
                .ofFloat(starView, "y", y);
        ObjectAnimator scaleXAnimator = ObjectAnimator
                .ofFloat(starView, "scaleX", 1);
        ObjectAnimator scaleYAnimator = ObjectAnimator
                .ofFloat(starView, "scaleY", 1);
        ObjectAnimator rotationAnimator = ObjectAnimator
                .ofFloat(starView, "rotation", angle);
        set.playTogether(xAnimator, yAnimator, scaleXAnimator, scaleYAnimator, rotationAnimator);
        set.setDuration(1500);
        return set;
    }

    public class WordsAdapter extends RecyclerView.Adapter<WordHolder> {

        @Override
        public WordHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.item_solved_word, parent, false);
            return new WordHolder(view);
        }

        @Override
        public void onBindViewHolder(WordHolder holder, int position) {
            holder.bind(solvedWords.get(position));
        }

        @Override
        public int getItemCount() {
            return solvedWords.size();
        }
    }

    public class WordHolder extends RecyclerView.ViewHolder {

        private final TextView solvedWord;

        public WordHolder(View itemView) {
            super(itemView);
            solvedWord = (TextView) itemView.findViewById(R.id.solved_word);
        }

        public void bind(Word word) {
            solvedWord.setText(word.word_correct);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(BACK_TO_MENU, null);
        finish();
    }

}
