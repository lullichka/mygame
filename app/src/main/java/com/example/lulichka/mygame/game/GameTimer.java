package com.example.lulichka.mygame.game;

import com.example.lulichka.mygame.messages.RoundEndMessage;
import com.example.lulichka.mygame.messages.TimerMessage;
import com.example.lulichka.mygame.util.SoundHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

public class GameTimer {

    private static GameTimer INSTANCE = new GameTimer();
    private Timer timer;
    boolean isCountdownStarted;
    public int millisLeft;
    public boolean isRunning;
    private boolean isInBackground;
    private boolean isPaused;

    public static GameTimer getInstance() {
        return INSTANCE;
    }

    private GameTimer() {
    }

    public void start() {
        if (isRunning) {
            return;
        }
        isRunning = true;

        this.millisLeft = 35 * 1000;
        //this.millisLeft = 120 * 1000;

        timer = new Timer(true);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (isPaused) {
                    return;
                }
                millisLeft -= 20;
                if (isInBackground) {
                    return;
                }
                if (millisLeft <= 10_000 && !isCountdownStarted) {
                    SoundHelper.getInstance(null).playCountdown();
                    isCountdownStarted = true;
                }
                if (millisLeft % 1000 == 0) {
                    TimerMessage timerMessage = new TimerMessage();
                    timerMessage.setFrozen(false);
                    timerMessage.setSecondsLeft(millisLeft / 1000);
                    EventBus.getDefault().post(timerMessage);
                }
                if (millisLeft <= 0) {
                    EventBus.getDefault().post(new RoundEndMessage());
                    stop();
                }
            }
        }, 0, 20);
    }

    public void pause() {
        isPaused = true;
    }

    public void resume() {
        isPaused = false;
    }

    public int getTimeLeft() {
        return millisLeft;
    }

    public void stop() {
        isRunning = false;
        isCountdownStarted = false;
        SoundHelper.getInstance(null).pauseCountdown();
        if (timer != null) {
            timer.cancel();
        }
    }

    public void setInBackground(boolean inBackground) {
        isInBackground = inBackground;
    }
}
