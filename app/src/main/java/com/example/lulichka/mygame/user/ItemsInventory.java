package com.example.lulichka.mygame.user;

import android.content.Context;
import android.content.SharedPreferences;


public class ItemsInventory {
    private static final String POINTS = "points";
    private static ItemsInventory INSTANCE;
    private final SharedPreferences sharedPreferences;
    private Context context;

    ItemsInventory(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("INVENTORY", Context.MODE_PRIVATE);
    }

    public static ItemsInventory getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("ItemsInventory wasn't initialized");
        }
        return INSTANCE;
    }

    public static void init(Context context) {
        INSTANCE = new ItemsInventory(context);
    }

    public void setPoints(int points) {
        getEditor().putInt(POINTS, points).apply();
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    public int getPoints() {
        return readValue(POINTS);
    }

    private int readValue(String points) {
        return sharedPreferences.getInt(points, 0);
    }

    private void usePointsLocally(int amount) {
        int availablePoints = getPoints();
        spendItemLocally(amount, availablePoints, POINTS);
    }

    private void spendItemLocally(int amount, int availableItems, String type) {
        if (availableItems - amount < 0) {
            throw new IllegalArgumentException("No sufficient amount of items");
        } else {
            String typeKey = POINTS;
            getEditor().putInt(typeKey, availableItems - amount).apply();
        }
    }

    public void addPoints(int amount) {
        usePoints(-amount);
    }

    private void usePoints(int amount) {
        if (getPoints() < amount) {
            amount = getPoints();
        }
        usePointsLocally(amount);
    }

}
