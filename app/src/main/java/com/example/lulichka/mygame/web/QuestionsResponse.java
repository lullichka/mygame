package com.example.lulichka.mygame.web;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class QuestionsResponse {

    public Data data;

    public static class Data {

        @JsonProperty("category")
        public List<Category> categories;

        public static class Category {

            public int id;

            public String name;

            public List<Word> words;

            @JsonIgnore
            public boolean isSolved;

            public static class Word {

                public int id;

                public String word;

                public String word_correct;

                @JsonIgnore
                public boolean isSolved;
            }
        }
    }
}
