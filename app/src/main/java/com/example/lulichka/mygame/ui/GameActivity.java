package com.example.lulichka.mygame.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lulichka.mygame.R;
import com.example.lulichka.mygame.game.GameManager;
import com.example.lulichka.mygame.game.GameTimer;
import com.example.lulichka.mygame.game.WordsRepository;
import com.example.lulichka.mygame.messages.GameStartedMessage;
import com.example.lulichka.mygame.messages.SkipWordMessage;
import com.example.lulichka.mygame.messages.WordSolvedMessage;
import com.example.lulichka.mygame.user.ItemsInventory;
import com.example.lulichka.mygame.util.SoundHelper;
import com.example.lulichka.mygame.web.QuestionsResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameActivity extends AppCompatActivity {
    private static final long SPLASH_DURATION = 2000;
    private SoundHelper soundHelper;
    @BindView(R.id.timer_view)
    TimerView timerView;
    @BindView(R.id.button_settings)
    ImageView settings;
    @BindView(R.id.counter)
    TextView counterPoints;

    @OnClick(R.id.button_settings)
    public void openSettings() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, new SettingsFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);
        ButterKnife.bind(this);
        ItemsInventory itemsInventory = ItemsInventory.getInstance();
        counterPoints.setText(String.valueOf(itemsInventory.getPoints()));
        soundHelper = SoundHelper.getInstance(this);
        EventBus.getDefault().register(this);
        if (fragment == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_container, new GameFragment())
                    .addToBackStack(null)
                    .commit();
        }
    }

    public static void start(Context context) {
        Intent starter = new Intent(context, GameActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ItemsInventory itemsInventory = ItemsInventory.getInstance();
        counterPoints.setText(String.valueOf(itemsInventory.getPoints()));
    }

    @Subscribe
    public void onGameStarted(GameStartedMessage message) {
        GameTimer instance = GameTimer.getInstance();
        instance.start();
        timerView.setTime(instance.millisLeft / 1000);
        timerView.setProperTimerColor();
        timerView.setVisibility(View.VISIBLE);
        settings.setVisibility(View.GONE);
    }
    @Override
    protected void onStop() {
        super.onStop();
        GameTimer.getInstance().setInBackground(true);
        soundHelper.pauseAllSounds();
    }

    @Override
    protected void onStart() {
        super.onStart();
        GameTimer.getInstance().setInBackground(false);
        soundHelper.resumeAllSounds();
    }

    @Subscribe
    public void onWordSolved(WordSolvedMessage message) {
        QuestionsResponse.Data.Category.Word wordById =
                WordsRepository.getInstance().getWordById(message.solvedWordId);
        if (wordById != null) {
            int pointsGain = wordById.word.length() > 5 ? 50 : 25;
            ItemsInventory instance = ItemsInventory.getInstance();
            GameManager gameManager = GameManager.getInstance();
            if (!gameManager.isLastRound()) {
                new Handler().postDelayed(this::startNewRound, SPLASH_DURATION);
            }
            int gameCompensation = gameManager.getGameCompensation();
            if (gameCompensation > 0) {
                int gameCompensationRemaining = gameCompensation - pointsGain;
                gameManager.setGameCompensation(gameCompensationRemaining);
                if (gameCompensationRemaining <= 0) {
                    instance.addPoints(-gameCompensationRemaining);
                }
            } else {
                instance.addPoints(pointsGain);
            }
        }
    }

    @Subscribe
    public void onSkipWord(SkipWordMessage message) {
        GameManager gameManager = GameManager.getInstance();
        if (!gameManager.isLastRound()) {
            new Handler().postDelayed(this::startNewRound, SPLASH_DURATION);
        }
    }

    private void startNewRound() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.fragment_container, new GamePageFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    @Override
    public void onBackPressed() {
    }
}
