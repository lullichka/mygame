package com.example.lulichka.mygame.ui;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lulichka.mygame.R;
import com.example.lulichka.mygame.game.WordsRepository;
import com.example.lulichka.mygame.util.UiUtils;
import com.example.lulichka.mygame.web.QuestionsResponse;
import com.example.lulichka.mygame.web.RestService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GameFragment extends Fragment {
    private static final String TAG = GameFragment.class.getSimpleName();
    @BindView(R.id.category_list)
    RecyclerView categories;
    private CategoriesAdapter categoriesAdapter;

    public GameFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_game, container, false);
        ButterKnife.bind(this, inflate);
        categories.setLayoutManager(new LinearLayoutManager(getActivity()));
        categoriesAdapter = new CategoriesAdapter();
        categories.setAdapter(categoriesAdapter);
        if (WordsRepository.getInstance().getAllCategories().isEmpty()) {
            loadQuestions();
        } else
            categoriesAdapter.setCategories(WordsRepository.getInstance().getAllCategories());
        return inflate;
    }


    private void loadQuestions() {
        UiUtils.showLoading(getActivity());
        RestService.Service.getQuestions().enqueue(new Callback<QuestionsResponse>() {
            @Override
            public void onResponse(Call<QuestionsResponse> call, Response<QuestionsResponse> response) {
                UiUtils.hideLoading();
                if (response.isSuccessful()) {
                    QuestionsResponse body = response.body();
                    if (body.data != null
                            && body.data.categories != null) {
                        saveToRepository(body.data.categories);
                        categoriesAdapter.setCategories(body.data.categories);
                        categoriesAdapter.notifyDataSetChanged();
                        Log.d(TAG, body.data.categories.toString());
                    }
                } else {
                    Toast.makeText(getActivity(), "Couldn't connect to server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<QuestionsResponse> call, Throwable t) {
                UiUtils.hideLoading();
                Log.d(TAG, t.getMessage());
            }
        });
    }

    private void saveToRepository(List<QuestionsResponse.Data.Category> categories) {
        WordsRepository.getInstance().saveQuestions(categories);
    }

    public class CategoriesAdapter extends RecyclerView.Adapter<CategoryHolder> {
        private List<QuestionsResponse.Data.Category> categories = new ArrayList<>();

        @Override
        public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            return new CategoryHolder(layoutInflater.inflate(R.layout.item_category, parent, false));
        }

        void setCategories(List<QuestionsResponse.Data.Category> categories) {
            this.categories.clear();
            this.categories.addAll(categories);
        }

        @Override
        public void onBindViewHolder(CategoryHolder holder, int position) {
            holder.bind(categories.get(position));
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.category_title)
        TextView categoryTitle;
        @BindView(R.id.card_category)
        CardView cardView;

        int categoryId;

        @OnClick(R.id.card_category)
        void onCategorySelected() {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_container, GamePageFragment.newInstance(categoryId))
                    .addToBackStack(null)
                    .commit();
        }

        CategoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Cartwheel.otf");
            categoryTitle.setTypeface(tf);
        }

        void bind(QuestionsResponse.Data.Category category) {
            categoryTitle.setText(category.name);
            categoryId = category.id;
            /*category.isSolved = PreferenceHelper.getInstance().isCategorySolved();
            if (category.isSolved){
                cardView.setCardBackgroundColor(Color.BLUE);
            }*/
        }
    }
}
