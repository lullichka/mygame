package com.example.lulichka.mygame.ui;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lulichka.mygame.R;
import com.example.lulichka.mygame.game.GameManager;
import com.example.lulichka.mygame.game.GameTimer;
import com.example.lulichka.mygame.game.WordsRepository;
import com.example.lulichka.mygame.messages.GameStartedMessage;
import com.example.lulichka.mygame.messages.RoundEndMessage;
import com.example.lulichka.mygame.messages.SkipWordMessage;
import com.example.lulichka.mygame.messages.WordSolvedMessage;
import com.example.lulichka.mygame.user.ItemsInventory;
import com.example.lulichka.mygame.util.SoundHelper;
import com.example.lulichka.mygame.web.QuestionsResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class GamePageFragment extends Fragment {

    private static final String CATEGORY_ID = "categoryId";
    private static final int RESULT_ACTIVITY = 1243;
    private static final String TAG = GamePageFragment.class.getSimpleName();
    private static final long SPLASH_DURATION = 2000;

    @BindView(R.id.game_field)
    GameField gameField;
    @BindView(R.id.category_title)
    TextView categoryTitle;
    @BindView(R.id.tv_user_points_counter)
    TextView pointsCounter;
    @BindView(R.id.tv_user_gain_points)
    TextView pointsGain;

    @BindDimen(R.dimen.gain_score_translation)
    float translation;
    private AlertDialog exitDialog;

    private GameActivity gameActivity;
    private GameManager gameManager;
    private int mCategoryId;

    @OnClick(R.id.btn_back)
    public void onBackToMenuCategoriesPressed() {
        showExitDialog();
        gameActivity.timerView.setVisibility(View.GONE);
    }

    public void showExitDialog() {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_exit, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setCancelable(true);
        builder.setView(view);
        exitDialog = builder.create();
        view.findViewById(R.id.cancel_exit).setOnClickListener(v -> exitDialog.dismiss());
        view.findViewById(R.id.confirm_exit).setOnClickListener(v -> {
            SoundHelper.getInstance(getActivity()).stopBackgroundMusic();
            GameTimer.getInstance().stop();
            gameActivity.settings.setVisibility(View.VISIBLE);
            FragmentTransaction transaction = getActivity()
                    .getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_container, new GameFragment())
                    .commit();
            exitDialog.dismiss();
        });
        TextView message = (TextView) view.findViewById(R.id.message);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Cartwheel.otf");
        message.setTypeface(tf);
        exitDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        exitDialog.show();
    }

    @OnClick(R.id.btn_skip)
    public void onSkipWordPressed() {
        EventBus.getDefault().post(new SkipWordMessage());
    }

    public GamePageFragment() {
        // Required empty public constructor
    }

    public static GamePageFragment newInstance(int categoryId) {
        GamePageFragment fragment = new GamePageFragment();
        Bundle args = new Bundle();
        args.putInt(CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategoryId = getArguments().getInt(CATEGORY_ID);
            gameManager = GameManager.getInstance();
            gameManager.startNewGame(mCategoryId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_game_page, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        gameActivity = (GameActivity) getActivity();
        Typeface tf = Typeface.createFromAsset(gameActivity.getAssets(), "fonts/Cartwheel.otf");
        categoryTitle.setTypeface(tf);
        startRound();
        return view;
    }


    private void setQuestion(QuestionsResponse.Data.Category.Word word, GameField gameField) {
        gameField.setWord(word);
    }

    @Subscribe
    public void onWordSolved(WordSolvedMessage message) {
        int change = 0;
        QuestionsResponse.Data.Category.Word wordById =
                WordsRepository.getInstance().getWordById(message.solvedWordId);
        if (wordById != null) {
            change = wordById.word.length() > 5 ? 50 : 25;
        }
        animatePointsChange(change);
        showGainPointsAnimation(change);
        SoundHelper.getInstance(getContext()).playPointSound();
        WordsRepository wordsRepository = WordsRepository.getInstance();
        wordsRepository.setWordSolved(message.solvedWordId);
        GameManager gameManager = GameManager.getInstance();
        gameManager.onWordSolved(change);
        if (!gameManager.isLastRound()) {
            gameManager.nextRound();
        } else {
            GameTimer.getInstance().stop();
            new Handler().postDelayed(this::showResults, SPLASH_DURATION);
        }
    }

    @Subscribe
    public void onSkipWord(SkipWordMessage message) {
        GameManager gameManager = GameManager.getInstance();
        if (!gameManager.isLastRound()) {
            gameManager.nextRound();
        } else {
            GameTimer.getInstance().stop();
            new Handler().postDelayed(this::showResults, SPLASH_DURATION);
        }
    }

    private void animatePointsChange(int change) {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(change);
        valueAnimator.setDuration(1000);
        valueAnimator.addUpdateListener(animation -> {
            Integer pointsToDisplay = GameManager.getInstance().getPointsToDisplay();
            Integer integer = (Integer) animation.getAnimatedValue();
            pointsCounter.setText(String.valueOf(pointsToDisplay + integer));
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                Integer pointsToDisplay = GameManager.getInstance().getPointsToDisplay();
                GameManager.getInstance().setPointsToDisplay(pointsToDisplay + change);
            }
        });
        valueAnimator.start();
    }

    private void showGainPointsAnimation(int change) {
        pointsGain.setAlpha(1f);
        pointsGain.setText("+" + change);
        pointsGain.setVisibility(View.VISIBLE);
        ObjectAnimator alphaChange = ObjectAnimator.ofFloat(pointsGain, "alpha", 0);
        ObjectAnimator translationYChange = ObjectAnimator.ofFloat(pointsGain, "translationY", -translation);
        ObjectAnimator scaleXChange = ObjectAnimator.ofFloat(pointsGain, "scaleX", 2);
        ObjectAnimator scaleYChange = ObjectAnimator.ofFloat(pointsGain, "scaleY", 2);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                alphaChange,
                translationYChange,
                scaleXChange,
                scaleYChange
        );
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                pointsGain.setTranslationY(0);
                pointsGain.setScaleX(1 / 2);
                pointsGain.setScaleY(1 / 2);
                pointsGain.setVisibility(View.GONE);
            }
        });
        animatorSet.setDuration(800);
        animatorSet.start();
    }

    void startRound() {
        GameManager gameManager = GameManager.getInstance();
        if (gameManager.getCurrentWordIndex() == 0) {
            setUpPointValue();
            ItemsInventory itemsInventory = ItemsInventory.getInstance();
            int points = itemsInventory.getPoints();
            gameManager.setGameCompensation(points < 100 ? 100 - points : 0);
            itemsInventory.addPoints(-100);
            SoundHelper.getInstance(getActivity()).playBackgroundMusic();
        }
        Integer pointsToDisplay = GameManager.getInstance().getPointsToDisplay();
        pointsCounter.setText(String.valueOf(pointsToDisplay));
        EventBus.getDefault().post(new GameStartedMessage());
        showCategory();
        QuestionsResponse.Data.Category.Word word = gameManager.getCurrentWord();
        setQuestion(word, gameField);
    }

    private void setUpPointValue() {
        GameManager.getInstance().setPointsToDisplay(0);
    }

    void showCategory() {
        categoryTitle.setSelected(true);
        GameManager gameManager = GameManager.getInstance();
        String catTitle = gameManager.getCurrentCategory().name;
        categoryTitle.setText(catTitle);
    }

    public void showResults() {
        SoundHelper soundHelper = SoundHelper.getInstance(null);
        soundHelper.stopBackgroundMusic();
        GameManager gameManager = GameManager.getInstance();
        QuestionsResponse.Data.Category category = gameManager.getCurrentCategory();
        mCategoryId = category.id;
        Intent intent = new Intent(getActivity().getApplicationContext(), ResultActivity.class);
        intent.putExtra(CATEGORY_ID, mCategoryId);
        GameTimer.getInstance().stop();
        gameActivity.timerView.setVisibility(View.GONE);
        startActivityForResult(intent, RESULT_ACTIVITY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_ACTIVITY) {
            GameManager gameManager = GameManager.getInstance();
            mCategoryId = data.getIntExtra(CATEGORY_ID, 0);
            switch (resultCode) {
                case ResultActivity.NEXT_GAME:
                    mCategoryId = gameManager.getNextCategoryId();
                    gameManager.startNewGame(mCategoryId);
                    FragmentTransaction transaction0 = getActivity()
                            .getSupportFragmentManager().beginTransaction();
                    transaction0.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_container, GamePageFragment.newInstance(mCategoryId))
                            .commit();
                    SoundHelper.getInstance(getActivity()).playBackgroundMusic();
                    break;
                case ResultActivity.BACK_TO_MENU:
                    gameActivity.settings.setVisibility(View.VISIBLE);
                    FragmentTransaction transaction = getActivity()
                            .getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_container, new GameFragment())
                            .commit();
                    break;
                case ResultActivity.REPLAY:
                    SoundHelper.getInstance(getActivity()).playBackgroundMusic();
                    gameManager.startNewGame(mCategoryId);
                    FragmentTransaction transaction1 = getActivity()
                            .getSupportFragmentManager().beginTransaction();
                    transaction1.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_container, GamePageFragment.newInstance(mCategoryId))
                            .commit();
                    break;
            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTimeEnd(RoundEndMessage message) {
        if (exitDialog != null && exitDialog.isShowing()) {
            exitDialog.dismiss();
        }
        showResults();
    }

}
